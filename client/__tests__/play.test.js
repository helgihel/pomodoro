import React from 'react';
import { render } from '@testing-library/react';
import { App } from '../src/App';

test('it contains play text', () => {
  const { getByText } = render(<App />);

  const playBtn = getByText('Play');
  expect(playBtn).toBeTruthy();
});
