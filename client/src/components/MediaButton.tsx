import React, { FunctionComponent } from 'react';
import { Button } from './Button';

type ActionButtonProps = {
  onClick: () => void;
  children: object;
};

export const MediaButton: FunctionComponent<ActionButtonProps> = ({
  onClick,
  children,
}) => {
  return <Button onClick={onClick}>{children}</Button>;
};
