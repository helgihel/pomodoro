import React, { useEffect, FunctionComponent } from 'react';
import styled from '@emotion/styled';

type TimeProps = {
  isActive?: boolean;
  timeLeft: number;
};

const Wrapper = styled.div`
  grid-column-start: 1;
  grid-row-start: 2;
  display: grid;
  font-size: 2.5em;
  width: 100%;
  align-content: center;
  justify-content: center;
`;

const Time: FunctionComponent<TimeProps> = ({ isActive, timeLeft }) => {
  const minutes = Math.floor(timeLeft / 60);
  const seconds = timeLeft - minutes * 60;

  return (
    <Wrapper>
      {minutes.toString().length < 2 ? `0${minutes}` : minutes} :{' '}
      {seconds.toString().length < 2 ? `0${seconds}` : seconds}
    </Wrapper>
  );
};

export { Time };
