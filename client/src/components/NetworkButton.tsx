import React, { FunctionComponent } from 'react';
import styled from '@emotion/styled';

type NetworkButtonProps = {
  onClick: () => void;
  text: string;
};

export const NetworkButton: FunctionComponent<NetworkButtonProps> = ({
  onClick,
  text,
}) => {
  return <Wrapper onClick={onClick}>{text}</Wrapper>;
};

export const Wrapper = styled.button`
  background-color: #fc0453;
  border: none;
  color: #fff;
  height: 2.2em;
  font-size: 1.2em;
  border-radius: 10px;
`;
