import React from 'react';
import styled from '@emotion/styled';

export const Background = styled.div`
  background-color: var(--backgroundColor);
  height: 100vh;
  width: 100vw;
  padding: 20px;
  display: grid;
  grid-template-columns: 10% auto 10%;
  grid-template-rows: 5% auto 10%;
`;
