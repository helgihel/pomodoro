import React from 'react';
import styled from '@emotion/styled';

const Image = styled.div`
width: 140px;
background-image: url('${require('../assets/images/tomato.png')}');
background-repeat: no-repeat;
background-position: center;
background-size: contain;
`;

const LogoContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const Logo = () => (
  <LogoContainer>
    <Image />
  </LogoContainer>
);
