import React from 'react';
import styled from '@emotion/styled';

export const Button = styled.button`
  background-color: #fc0453;
  border: none;
  height: 48px;
  width: 48px;
  border-radius: 25px;
  color: inherit;
`;
