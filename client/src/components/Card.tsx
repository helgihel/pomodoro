import styled from '@emotion/styled';

export const Card = styled.div`
  background-color: var(--backgroundColor);
  border: 1px solid var(--border);
  box-shadow: inset 0 0 4px var(--border);
  height: 30em;
  width: 18em;
  min-height: 600px;
  min-width: 320px;
  border-radius: 5px;
  grid-column-start: 2;
  grid-row-start: 2;
  margin: auto;
  display: grid;
  grid-auto-columns: 15em;
  grid-template-rows: 2fr 1fr 1fr;
  justify-content: center;
  align-content: center;
`;
