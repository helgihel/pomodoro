import React, { useEffect } from 'react';
import { injectGlobal } from 'emotion';
import { Timer } from './containers/Timer';
import { Background, Card, Logo } from './components';
import { NetworkComponents } from './containers/NetworkComponents';

export const App = () => {
  useEffect(() => {
    injectGlobal`
      @font-face {
      font-family: 'RacingSansOne-Regular';
      src: url('${require('./assets/fonts/sd prostreet.ttf')}') format('truetype');
    }

    :root {
      --backgroundColor: #181818;
      --primaryColor: #94C595;
      --secondaryColor: #747C92;
      /* --fontColor: #A1E8AF; */
      --fontColor: #9A4C95;
      --border: #9A4C95;
    }
    body {
      margin: 0px;
      font-family: 'RacingSansOne-Regular', -apple-system, Helvetica ,sans-serif;
      color: var(--fontColor);
    }
    `;
  }, []);
  return (
    <Background>
      <Card>
        <Logo />
        <Timer />
        <NetworkComponents />
      </Card>
    </Background>
  );
};
