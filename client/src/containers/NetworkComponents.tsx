import React, { FunctionComponent } from 'react';
import styled from '@emotion/styled';
import { NetworkButton } from '../components/NetworkButton';

export const NetworkComponents: FunctionComponent = () => {
  return (
    <Wrapper>
      <NetworkButton text="Save" onClick={() => console.log('Saving')} />
      <NetworkButton text="Delete" onClick={() => console.log('Deleting')} />
    </Wrapper>
  );
};

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
