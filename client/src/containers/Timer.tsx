import React, { useState, useEffect } from 'react';
import { Time, MediaButton } from '../components';

// svg icons
import Play from '../assets/icons/play.svg';
import Stop from '../assets/icons/stop.svg';
import Pause from '../assets/icons/pause.svg';
import styled from '@emotion/styled';

// 25 minutes = 1500
// 5 minutes = 300

// 25 minutes
const sessionLength = 1500;
// 5 minutes
const shortBreakLength = 300;
// 25 minutes
const longBreakLength = 1500;

export const Timer = () => {
  const [isActive, setActive] = useState<boolean>(false);
  const [isInBreak, setBreack] = useState<boolean>(false);
  const [timeLeft, setTimeLeft] = useState<number>(sessionLength);
  const [shortSessionCount, setShortSessionCount] = useState<number>(0);
  const [longSessionCount, setLongSessionCount] = useState<number>(0);

  useEffect(() => {
    if (timeLeft < 1) {
      if (!isInBreak) {
        if (shortSessionCount < 4) {
          setShortSessionCount(shortSessionCount + 1);
          setBreack(true);
          setTimeLeft(shortBreakLength);
        } else if (shortSessionCount === 4) {
          setShortSessionCount(0);
          setLongSessionCount(longSessionCount + 1);
          setBreack(true);
          setTimeLeft(longBreakLength);
        }
      } else {
        setBreack(false);
        setTimeLeft(sessionLength);
      }
    }
    if (timeLeft < 1) {
      setActive(false);
    }
    if (isActive) {
      const interval = setInterval(() => {
        setTimeLeft(timeLeft - 1);
      }, 1000);
      return () => {
        if (interval) {
          clearInterval(interval);
        }
      };
    }
  }, [timeLeft, isActive]);
  const startSession = () => {
    setActive(true);
  };
  const pauseSession = () => {
    setActive(false);
  };
  const resetTimer = () => {
    setActive(false);
    setLongSessionCount(0);
    setShortSessionCount(0);
  };
  const stopSession = () => {
    resetTimer();
  };

  return (
    <div>
      <Time timeLeft={timeLeft} isActive={isActive} />
      <ActionButtonWrapper>
        <MediaButton onClick={() => startSession()}>
          <Play />
        </MediaButton>
        <MediaButton onClick={() => pauseSession()}>
          <Stop />
        </MediaButton>
        <MediaButton onClick={() => stopSession()}>
          <Pause />
        </MediaButton>
      </ActionButtonWrapper>
    </div>
  );
};

const ActionButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 30px;
`;
